package com.example.nabilla.firebasecrud.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nabilla.firebasecrud.R;
import com.example.nabilla.firebasecrud.model.User;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder>{

    private Context context;
    private List<User> userList = new ArrayList<>();

    public RecyclerAdapter(Context context, List<User> userList) {
        this.context = context;
        this.userList = userList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_data, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        User user = userList.get(position);

        holder.tvNama.setText(user.getNama());
        holder.tvJenisKelamin.setText(user.getJenisKelamin());
        holder.tvAlamat.setText(user.getAlamat());
        Picasso.with(context)
                .load(user.getUrl())
                .into(holder.ivProfile);
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvNama, tvAlamat, tvJenisKelamin;
        ImageView ivProfile;

        public ViewHolder(View itemView) {
            super(itemView);

            tvAlamat = (TextView) itemView.findViewById(R.id.tvAlamat);
            tvNama = (TextView) itemView.findViewById(R.id.tvNama);
            tvJenisKelamin = (TextView) itemView.findViewById(R.id.tvJenisKelamin);
            ivProfile = (ImageView) itemView.findViewById(R.id.ivProfile);
        }
    }
}
