package com.example.nabilla.firebasecrud;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginAndSignup extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    EditText edtUname,edtPass;
    String name, pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_and_signup);

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                } else {
                    // User is signed out
                }
            }
        };

        edtUname = (EditText) findViewById(R.id.edtUname);
        edtPass = (EditText) findViewById(R.id.edtPass);
    }

    public void Masuk(View view) {
        name = edtUname.getText().toString();
        pass = edtPass.getText().toString();

        mAuth.signInWithEmailAndPassword(name, pass)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (!task.isSuccessful()) {
                            Toast.makeText(LoginAndSignup.this, "Alamat Email tidak tersedia",
                                    Toast.LENGTH_SHORT).show();
                            Log.w("TEST", "signInWithEmail:failed", task.getException());
                        }else{
                            startActivity(new Intent(LoginAndSignup.this, MainActivity.class));
                            finish();
                        }
                    }
                });
    }

    public void Mendaftar(View view) {
        name = edtUname.getText().toString().trim();
        pass = edtPass.getText().toString();

        mAuth.createUserWithEmailAndPassword(name, pass)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (!task.isSuccessful()) {
                            Toast.makeText(LoginAndSignup.this, "Gagal Sign Up",
                                    Toast.LENGTH_SHORT).show();
                            edtPass.setText("");
                            edtUname.setText("");
                        }else{
                            Toast.makeText(LoginAndSignup.this, "Berhasil ! Silahkan login",
                                    Toast.LENGTH_SHORT).show();
                            edtPass.setText("");
                            edtUname.setText("");
                        }

                    }
                });
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }
}
