package com.example.nabilla.firebasecrud;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.example.nabilla.firebasecrud.model.User;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    EditText edtNama;
    EditText edtAlamat;
    ImageView ivProfile;
    AdView adView;
    RadioButton rbLaki;
    String id;
    DatabaseReference databaseReference;
    StorageReference storageReference;
    FirebaseStorage storage;
    private static int PICK_IMAGE_RESULT = 1;
    String dataa;
    Uri uri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtAlamat = (EditText) findViewById(R.id.edtAlamat);
        edtNama = (EditText) findViewById(R.id.edtNama);
        rbLaki = (RadioButton) findViewById(R.id.rbLaki);
        ivProfile = (ImageView) findViewById(R.id.ivProfile);
        adView = (AdView) this.findViewById(R.id.adView);

        databaseReference = FirebaseDatabase.getInstance().getReference();
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        MobileAds.initialize(this, getResources().getString(R.string.banner_ad_unit_id) );

        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("BEDA38DE72F2A0725AFC3D3F7F7DB1F7")
                .build();

        adView.loadAd(adRequest);

        id = databaseReference.push().getKey();
    }

    public void UnggahGambar(View view) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Pilih Gambar"), PICK_IMAGE_RESULT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_RESULT && resultCode == RESULT_OK){
            uri = data.getData();
            dataa = String.valueOf(uri);

            try{
                Bitmap bitmap =  MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                ivProfile.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void simpanData(View view) {
        String alamat = edtAlamat.getText().toString();
        String nama = edtNama.getText().toString();
        String jenkel = "";
        if (rbLaki.isChecked()){
            jenkel = "Laki-laki";
        }else{
            jenkel = "Perempuan";
        }

        if (!TextUtils.isEmpty(alamat) || !TextUtils.isEmpty(nama)){
            User user = new User(nama, alamat, jenkel, dataa);
            databaseReference.child("users").child(id).setValue(user);
            Toast.makeText(MainActivity.this, "Berhasil", Toast.LENGTH_LONG).show();
            ivProfile.setImageResource(R.mipmap.ic_launcher);
            edtNama.setText("");
            edtAlamat.setText("");
            rbLaki.setChecked(true);
        }else{
            Toast.makeText(MainActivity.this, "Lengkapi data!", Toast.LENGTH_LONG).show();
        }
        uploadImage();
    }

    private void uploadImage(){
        Log.d("TEST", String.valueOf(uri));
        ivProfile.setDrawingCacheEnabled(true);
        ivProfile.buildDrawingCache();
        Bitmap bitmap = ivProfile.getDrawingCache();
        ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArray);
        byte[] data = byteArray.toByteArray();

        StorageReference sf = storageReference.child("image/"+ id);
        UploadTask uploadTask = sf.putFile(uri);

        final ProgressDialog pg = new ProgressDialog(this);
        pg.setMessage("Mengupload Gambar..");

        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                @SuppressWarnings("VisibleForTests")
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                Log.d("Test", String.valueOf(downloadUrl));
                pg.cancel();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.lihat_data){
            startActivity(new Intent(this, DetailActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }
}
